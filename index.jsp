                     <md-input-container class="md-block">
                     	<input type="email" name="hotelEmail" ng-model="text" ng-pattern="emailFormat" required>
                        <span class="error" ng-show="userRegistration.hotelEmail.$error.required">
                        Required!
                        </span>
                        <span class="error" ng-show="userRegistration.hotelEmail.$error.pattern">
                        Not valid email!
                         </span>
                     </md-input-container>